#!/bin/bash
set -euo pipefail
# this script makes the ground work on a debian-10 vm for installing a minecraft server later
# last passed test: Debian 10.0 netinst on July 14th 2019 
# run this script as root

# constants
SUDOER_AND_SSH_USERNAME=linus # new user for ssh connexion and sudo commands
SSH_USERNAME_TEMP_PASSWORD=test1234 # temp passwd for ssh user
SSH_CONFIG_PATH=/etc/ssh/sshd_config
NEW_SSH_PORT=22 # we leave ssh default port
LOGFILE=logfile.txt
# packages

apt update --fix-missing |& tee -a $LOGFILE
apt upgrade -y |& tee -a $LOGFILE 
apt install -y sudo openssh-server git |& tee -a $LOGFILE 
#apt install -y bridge-utils # bridge-utils is necessary to run a bridged adapter on Oracle VirtualBox

# create new user for ssh and sudo
useradd -m -c "SSH user" -s /bin/bash "$SUDOER_AND_SSH_USERNAME" |& tee -a $LOGFILE 
usermod -aG sudo "$SUDOER_AND_SSH_USERNAME" |& tee -a $LOGFILE # add user to sudoers
# TODO: set temp password
echo "$SUDOER_AND_SSH_USERNAME:$SSH_USERNAME_TEMP_PASSWORD" | chpasswd |& tee -a $LOGFILE
# force user to change password at first login
chage -d 0 "$SUDOER_AND_SSH_USERNAME" |& tee -a $LOGFILE 

# Create your ssh keys on your machine
# then upload your public key to the user ssh config dir before proceeding
read -p "Upload your public ssh key to the user config dir (i.e. ssh-copy-id) then press [Enter] key to continue..." 
# Authorize newly uploaded public key by uncommenting the necessary line
sed -i '/^#AuthorizedKeysFile.*/s/^#//' $SSH_CONFIG_PATH |& tee -a $LOGFILE

# secure ssh server
sed -i "s/^#Port 22/Port $NEW_SSH_PORT/" $SSH_CONFIG_PATH |& tee -a $LOGFILE # change ssh port 
sed -i 's/^#PermitRootLogin.*/PermitRootLogin no/' $SSH_CONFIG_PATH |& tee -a $LOGFILE # we can't login as root through ssh
sed -i "s/^#PasswordAuthentication.*/PasswordAuthentication no/" $SSH_CONFIG_PATH |& tee -a $LOGFILE  #we can't use password for ssh
echo "AllowUsers $SUDOER_AND_SSH_USERNAME" >> $SSH_CONFIG_PATH |& tee -a $LOGFILE # we must ssh as our newly created user
echo "Protocol 2" >> $SSH_CONFIG_PATH |& tee -a $LOGFILE # Protocol 1 is insecure

systemctl restart ssh |& tee -a $LOGFILE 

# setup ufw
apt install -y ufw |& tee -a $LOGFILE 
ufw default allow outgoing |& tee -a $LOGFILE 
ufw default deny incoming |& tee -a $LOGFILE 
ufw allow OpenSSH |& tee -a $LOGFILE 
ufw logging on |& tee -a $LOGFILE 
ufw enable |& tee -a $LOGFILE 

# setup fail2ban
apt install -y fail2ban |& tee -a $LOGFILE 
# fail2ban defauls are reasonable so we use them in our fail2ban locale
cp /etc/fail2ban/fail2ban.conf /etc/fail2ban/fail2ban.local |& tee -a $LOGFILE 
service fail2ban restart|& tee -a $LOGFILE 

# switch to new user
# su - "$SUDOER_AND_SSH_USERNAME" |& tee -a $LOGFILE 



